package data_structures_test;

import model.data_structures.Stack;

import java.util.Iterator;

import junit.framework.TestCase;

/**
 * Clase de pruebas para la clase Stack.
 */
public class StackTest extends TestCase
{
	/**
	 * Stack de prueba.
	 */
	private Stack<String> stack;

	/**
	 * Escenario 1: Construye una nueva pila vac�a.
	 */
	public void setUpStage1()
	{
		stack = new Stack<String>();
	}

	/**
	 * Escenario 2: Construye una nueva pila y le a�ade ciertos elementos.
	 */
	public void setUpStage2()
	{
		stack = new Stack<String>();
		for( int i = 1; i < 11 ; i++)
		{
			stack.push(""+i);
		}
	}

	/**
	 * Prueba 1: Prueba el m�todo size de la clase Stack. <br>
	 * <b>M�todos a probar:</b> <br>
	 * size<br>
	 * <b> Casos de prueba: </b><br>
	 * 1. La pila no tiene elementos.<br>
	 * 2. Deber�a haber 10 elementos en la pila.
	 */
	public void testSize()
	{
		//Caso de prueba 1: La pila no tiene elementos.
		setUpStage1();
		assertTrue("No deber�a haber elemmentos en la pila." , stack.size() == 0 );

		//Caso de preuba 2: Existen elementos en la pila.
		setUpStage2();
		assertTrue("Deber�a haber 10 elementos en la pila." , stack.size() == 10 );
	}

	/**
     * Prueba 2: Prueba el m�todo push de la clase Stack. <br>
     * <b>M�todos a probar:</b> <br>
     * push<br>
     * <b> Casos de prueba: </b><br>
     * 1. Agrega elementos a una pila vac�a.<br>
     * 2. Agrega elementos a una pila que no est� vac�a.
     */
	public void testPush()
	{
		//Caso de prueba 1: Agrega elementos cuando la pila est� vac�a.
		setUpStage1();
		String line = "";
		for( int i = 0; i < 10 ; i++ )
		{
			line = line + "a";
			stack.push(line);
			assertTrue("Deber�an haber "+i+1+" elementos en la pila." , stack.size() == i+1 );
		}

		//Caso de prueba 2: Agrega elementos a una pila que no est� vac�a.
		setUpStage2();
		for( int i = 11 ; i <= 20 ; i++ )
		{
			stack.push(i+"");
			assertTrue("Deber�an haber "+i+" elementos en la pila." , stack.size() == i);
		}
	}

	/**
     * Prueba 3: Prueba el m�todo pop de la clase Stack. <br>
     * <b>M�todos a probar:</b> <br>
     * pop<br>
     * <b> Casos de prueba: </b><br>
     * 1. Eliminar desde una pila vac�a.<br>
     * 2. Ir vaciando una pila con elementos.
     */
	public void testPop()
	{
		//Caso de prueba 1: Eliminar desde una pila vac�a.
		setUpStage1();
		assertNull("No deber�a devolver elementos ya que est� vac�a." , stack.pop());

		//Caso de prueba 2: Ir vaciando una pila con elementos.
		setUpStage2();
		for( int i = 10 ; i > 0 ; i-- )
		{
			String c = stack.pop();
			assertTrue(" La pila deber�a devolver el elemento "+i+".", c.equals(""+i));
			assertTrue("El n�mero de elementos en la pila deber�a haber disminuido," , stack.size() == i-1);
		}
	}

	/**
     * Prueba 4: Prueba el m�todo iterator de la clase Stack. <br>
     * <b>M�todos a probar:</b> <br>
     * iterator<br>
     * <b> Casos de prueba: </b><br>
     * 1. Iterador en una pila vac�a.<br>
     * 2. Iterador en una pila con elementos.
     */
	public void testIterator()
	{
		//Caso de prueba 1: Iterador en una pila vac�a.
		setUpStage1();
		Iterator<String> iter = stack.iterator();
		while( iter.hasNext() )
		{
			fail("La pila est� vac�a, no deber�a haber entrado ene este punto.");
		}

		//Caso de prueba 2: Iterador en una pila con elementos.
		setUpStage2();
		iter = stack.iterator();
		int i = 10;
		while( iter.hasNext())
		{	
			String string = iter.next();
			assertTrue("Ambas cadenas deber�an coindidir." , string.equals(""+i));
			i--;
		}
	}


}
