package data_structures_test;

import model.data_structures.Queue;
import model.data_structures.Stack;

import java.util.Iterator;

import junit.framework.TestCase;

/**
 * Clase de pruebas para la clase Stack.
 */
public class QueueTest extends TestCase
{
	/**
	 * Queue de prueba.
	 */
	private Queue<Integer> queue;

	/**
	 * Escenario 1: Construye una nueva cola vac�a.
	 */
	public void setUpStage1()
	{
		queue= new Queue<Integer>();
	}


	 /**
     * Prueba 1: Prueba el m�todo constructor de la clase Queue. <br>
     * <b>M�todos a probar:</b> <br>
     * Queue<br>
     * isEmpty<br>
     * <b> Casos de prueba: </b><br>
     * 1. La cola fue creada y est� vacia.
     * 
     */
	public void testConstructor()
	{
		setUpStage1();
    	assertTrue("La cola debi� inicializarse vac�a", queue.isEmpty());
	}

	/**
     * Prueba 2: Prueba el m�todo enqueue de la clase Queue. <br>
     * <b>M�todos a probar:</b> <br>
     * enqueue<br>
     * getSize<br>
     * iterator<br>
     * <b> Casos de prueba: </b><br>
     * 1. Se agregaron 10 elementos a la cola y el tama�o de la cola corresponde.
     * 2. Se puede empezar a recorrer la cola con iterador
     * 3. El primer elemento que se agreg� coincide.
     * 4. El �ltimo elemento que se agreg� coincide.
     * 
     */
	public void testEnqueue()
	{
		setUpStage1();
    	int item=5;
    	for (int i=0; i<10;i++)
    	{
    		queue.enqueue(item);
    		item+=item;
    	}
    	assertEquals("La cola debe tener un tama�o de 10",10, queue.size());
    	Iterator<Integer> iter= queue.iterator();
    	assertTrue("La cola deber�a poder crear un iterador con elementos para recorrer", iter.hasNext());
    	assertEquals("El primer elemento deber�a ser 5",(Integer)5, iter.next());
    	Integer last=0;
    	while(iter.hasNext())
    	{
    		last=iter.next();
    	}
    	assertEquals("El �ltimo elemento deber�a ser 2560", (Integer)2560, last);
	}

	/**
     * Prueba 3: Prueba el m�todo dequeue de la clase Queue. <br>
     * <b>M�todos a probar:</b> <br>
     * dequeue<br>
     * getSize<br>
     * iterator<br>
     * <b> Casos de prueba: </b><br>
     * 1. El tama�o inicial de la cola corresponde (vac�a).
     * 2. Se elimina un elemento de la cola y retorna null.
     * 
     */
	public void testDequeue1()
	{
		setUpStage1();
    	assertEquals("La cola debe tener un tama�o de 0",0, queue.size());
    	Integer last=queue.dequeue();
    	assertNull("El eleento que debi� ser eliminado es 5", last);
	}

	/**
     * Prueba 4: Prueba el m�todo dequeue de la clase Queue. <br>
     * <b>M�todos a probar:</b> <br>
     * dequeue<br>
     * getSize<br>
     * <b> Casos de prueba: </b><br>
     * 1. El tama�o inicial de la cola corresponde.
     * 2. Se elimina un elemento de la cola y el tama�o corresponde.
     * 3. El elemento eliminado coincide con el primer elemento que se agreg�.
     * 
     */
	public void testDequeue2()
	{
		setUpStage1();
    	int item=5;
    	for (int i=0; i<10;i++)
    	{
    		queue.enqueue(item);
    		item+=5;
    	}
    	assertEquals("La cola debe tener un tama�o de 10",10, queue.size());
    	Integer last=queue.dequeue();
    	assertEquals("El eleento que debi� ser eliminado es 5",(Integer)5, last);
	}


}
