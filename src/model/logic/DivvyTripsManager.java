package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import com.sun.xml.internal.ws.util.StringUtils;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;

/**
 * Clase DivvyTripsManager
 */
public class DivvyTripsManager implements IDivvyTripsManager 
{
	/**
	 * Pila de viajes
	 */
	private Stack<VOTrip> tripsStack;

	/**
	 * Cola de viajes
	 */
	private Queue<VOTrip> tripsQueue;

	/**
	 * Pila de estaciones
	 */
	private Stack<VOStation> stationsStack;

	/**
	 * Constructor Divvy Trips Manager
	 */
	public DivvyTripsManager()
	{
		tripsStack = new Stack<VOTrip>();
		tripsQueue = new Queue<VOTrip>();
		stationsStack = new Stack<VOStation>();
	}

	/**
	 * Carga las estaciones desde un archivo recibido como par�metro.
	 */
	public void loadStations (String stationsFile) 
	{
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(stationsFile));
			br.readLine();
			String line = "";
			while((line = br.readLine())!= null && !line.isEmpty() )
			{
				String[] array = line.split(",");
				int id = Integer.parseInt(array[0]);
				String name = array[1];
				String city = array[2];
				double latitude = Double.parseDouble(array[3]);
				double longitude = Double.parseDouble(array[4]);
				int dpCapacity = Integer.parseInt(array[5]);
				String dateString = array[6];
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
				Date onlineDate = formatter.parse(dateString);
				
				VOStation station = new VOStation(id, name, city, latitude, longitude, dpCapacity, onlineDate);
				stationsStack.push(station);
			}
			System.out.println("Se han a�adido "+stationsStack.size()+" estaciones.");
			br.close();
		}
		catch( Exception e )
		{
			System.out.print("El archivo no tiene el formato esperado.");
		}
	}

	/**
	 * Carga los viajes desde un archivo recibido como par�metro.
	 */
	public void loadTrips (String tripsFile) 
	{
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(tripsFile));
			br.readLine();
			String line = "";
			while((line = br.readLine())!= null && !line.isEmpty() )
			{
				line=line.replaceAll("\"", "");
				String[] array = line.split(",");
				int id = Integer.parseInt(array[0]);
				String startTimeString = array[1];
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
				Date startTime = formatter.parse(startTimeString);
				String endTimeString = array[1];
				SimpleDateFormat formatterEnd = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
				Date endTime = formatterEnd.parse(endTimeString);
				int bike_id = Integer.parseInt(array[3]);
				double trip_seconds = Double.parseDouble(array[4]);
				int fromStationId = Integer.parseInt(array[5]);
				String fromStationName = array[6];
				int toStationId = Integer.parseInt(array[7]);
				String toStationName = array[8];
				String usertype = (array.length <= 9)? "Undefined usertype": array[9];
				String gender = (array.length <= 10)? "Undefined gender": array[10];
				String birthyear = (array.length <= 10)? "Undefined birthyear": array[10];
				

				VOTrip trip = new VOTrip(id, startTime, endTime, bike_id, trip_seconds, fromStationId, fromStationName, toStationId, toStationName, usertype, gender , birthyear);
				tripsQueue.enqueue(trip);
				tripsStack.push(trip);
			}
			System.out.println("Se han a�adido "+tripsQueue.size()+" viajes.");
			br.close();
		}
		catch( Exception e )
		{
			System.out.print("El archivo no tiene el formato esperado.");
		}
	}

	/**
	 * Devuelve en nombre de las �ltimas n estaciones a las que ha llegado una bicicleta.
	 */
	public IDoublyLinkedList <String> getLastNStations (int bicycleId, int n) 
	{
		DoublyLinkedList<String> answer =new DoublyLinkedList<String>();

		Iterator<VOTrip> iter=tripsQueue.iterator();
		while(iter.hasNext() && answer.getSize() <= n)
		{
			VOTrip trip = iter.next();
			if(trip.getBikeId() == bicycleId)
			{
				answer.addEnd(trip.getToStation());
			}
		}
		return answer;
	}

	/**
	 * Devuelve el n-�simo viaje que ha llegado a una estaci�n cuyo id se recibe como par�metro.
	 */
	public VOTrip customerNumberN (int stationID, int n) 
	{
		Iterator<VOTrip> iter=tripsQueue.iterator();
		int i=0;
		VOTrip trip = null;
		
		while(iter.hasNext() && i < n )
		{
			trip=iter.next();
			if(trip.getToStationId()==stationID)
			{
				i++;
			}
		}
		return (i == n)? trip : null ;
	}	


}
