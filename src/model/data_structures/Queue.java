package model.data_structures;

import java.util.Iterator;

public class Queue<E> implements IQueue<E>
{
	private int size;
	
	private Node<E> first;
	
	private Node<E> last;
	
	public Queue()
	{
		first = null;
		last = null;
		size = 0;
	}
	
	public Iterator<E> iterator() 
	{
		return new Iterator<E>() 
		{
			Node<E> actual = null;
			public boolean hasNext() 
			{
				if( size == 0 )
					return false;
				if( actual == null )
					return true;
				return ( actual.getNext() != null );
			}

			public E next() 
			{
				if( actual == null )
				{
					actual = first;
				}
				else
				{
					actual = actual.getNext();
				}
				return actual.getItem();
			}
		};
	}

	public boolean isEmpty() 
	{
		return (size == 0);
	}

	public int size() 
	{
		return size;
	}

	public void enqueue(E t) 
	{
		Node<E> node = new Node<E>(t);
		if( isEmpty() )
		{
			first = node;
			last = node;
		}
		else
		{
			last.setNext(node);
			last = node;
		}
		size++;
	}

	public E dequeue() 
	{
		if(isEmpty())
		{
			return null;
		}
		else
		{
			Node<E> node = first.getNext();
			E item = first.getItem();
			first.setNext(null);
			first = node;
			size--;
			return item;
		}
	}

}
