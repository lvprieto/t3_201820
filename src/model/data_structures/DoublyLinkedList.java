package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList<E> implements IDoublyLinkedList<E> {

	private Node<E> listStartNode;
	private Node<E> listEndNode;
	private int listSize=0;

	// La lista puede comenzar vacia	
	public DoublyLinkedList(){
		listStartNode=null;
		listEndNode=null;
	}

	// La lista puede comenzar con algun elemento
	public DoublyLinkedList(E item){
		listStartNode=new Node<E>(item);
		listEndNode=listStartNode;
		listSize=1;
	}

	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<E>(){
			Node<E> actual=null;
			public boolean hasNext(){
				if(listSize==0){
					return false;
				}
				if(actual ==null){
					return true;
				}
				return actual.getNext()!=null;
			}

			public E next(){
				if(actual==null){
					actual=listStartNode;
				}
				else{
					actual=actual.getNext();
				}
				return actual.getItem();
			}	
		};
	}

	@Override
	public void addFirst(E item) {
		// TODO Auto-generated method stub
		if(listSize==0){
			listStartNode= new Node<E>(item);
			listEndNode=listStartNode;
			listSize=1;
		}
		else{
			Node<E> newNode =new Node<E>(item);
			newNode.setNext(listStartNode);
			listStartNode.setBack(newNode);
			listStartNode=newNode;
			listSize ++;
		}
	}

	@Override
	public void addEnd(E item) {
		// TODO Auto-generated method stub
		if(listSize==0){
			listStartNode=new Node<E>(item);
			listEndNode=listStartNode;
			listSize=1;
		}
		else{
			Node<E> insertedNode=new Node<E>(item);
			listEndNode.setNext(insertedNode);
			insertedNode.setBack(listEndNode);
			listEndNode=insertedNode;
			listSize ++;
		}
	}
	//Se asume que las posiciones que recibe empiezan desde cero(0)
	@Override
	public void remove(int p) {
		// TODO Auto-generated method stub
		if(p<0||p>(listSize-1)){
			throw new ArrayIndexOutOfBoundsException("La posicion insertada "+p+" no se encuentra en la lista");
		}
		if(p==0){
			Node<E> nextNode=listStartNode.getNext();
			if(listStartNode.getNext()!=null){
				nextNode.setBack(null);
			}
			listStartNode.setNext(null);
			listStartNode=null;
			listStartNode=nextNode;
			listSize --;
		}
		else if(p==(listSize-1)){
			Node<E> lastNode=listEndNode.getBack();
			if(listEndNode.getBack()!=null){
				lastNode.setNext(null);
			}
			listEndNode.setBack(null);
			listEndNode=null;
			listEndNode=lastNode;
			listSize --;
		}
		else{
			Node<E> removeNode=listStartNode;
			for(int i=1;i<=p;i++){
				removeNode=removeNode.getNext();
			}
			Node<E> nextNode=removeNode.getNext();
			Node<E> backNode=removeNode.getBack();
			if(removeNode.getNext()!=null){	
				nextNode.setBack(backNode);
			}
			if(removeNode.getBack()!=null){
				backNode.setNext(nextNode);
			}
			removeNode.setBack(null);
			removeNode.setNext(null);
			listSize --;
		}

	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return listSize;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(listSize==0){
			return true;
		}
		else{
			return false;
		}
	}

	//Se asume que las posiciones que recibe empiezan desde cero(0)
	@Override
	public E getCurrentElement(int p) {
		// TODO Auto-generated method stub
		if(p<0||p>(listSize-1)){
			throw new ArrayIndexOutOfBoundsException("La posicion insertada "+p+" no se encuentra en la lista");
		}
		Node<E> currentNode=listStartNode;
		for(int i=1;i<=p;i++){
			currentNode=currentNode.getNext();
		}
		return currentNode.getItem();
	}

	

}
