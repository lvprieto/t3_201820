package model.data_structures;

import java.util.Iterator;

public class Stack<E> implements IStack<E>
{
	
	private Node<E> top;
	
	private int size;

	public Stack()
	{
		size=0;
		top=null;
	}
	
	public boolean isEmpty() 
	{
		return (size == 0);
	}

	public int size() 
	{
		return size;
	}

	public void push(E t) 
	{
		Node<E> node = new Node<E>(t);
		node.setNext(top);
		top = node;
		size++;
		
	}

	public E pop() 
	{
		if(isEmpty())
		{
			return null;
		}
		else
		{
			E item = top.getItem();
			Node<E> newTop = top.getNext();
			top.setNext(null);
			top = newTop;
			size--;
			return item;
		}
	}

	public Iterator<E> iterator() 
	{
		return new Iterator<E>() 
		{
			Node<E> actual = null;
			public boolean hasNext() 
			{
				if( size == 0 )
					return false;
				if( actual == null )
					return true;
				return ( actual.getNext() != null );
			}
	
			public E next() 
			{
				if( actual == null )
				{
					actual = top;
				}
				else
				{
					actual = actual.getNext();
				}
				return actual.getItem();
			}
		};
	}

}
