package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoublyLinkedList<E> extends Iterable<E> {

	//TODO Agregar API de DoublyLinkedList
	// A�adir un elemento a la lista
	public void addFirst(E item);
	public void addEnd(E item);

	// Quitar un elemento de la lista
	public void remove(int p);

	//Consultar el tama�o
	public int getSize();

	//Recuperar un elemento de la lista en una posicion
	public E getCurrentElement(int p);


	//Informa si la lista esta vacia
	public boolean isEmpty();


}
