package model.data_structures;

public class Node<E> 
{
	private Node<E> next ;
	private Node<E> back;
	
	private E item;
	
	public Node( E item )
	{
		this.item = item;
		next = null;
	}

	public Node<E> getNext() 
	{
		return next;
	}

	public void setNext(Node<E> next) 
	{
		this.next = next;
	}
	
	public Node<E> getBack() 
	{
		return back;
	}

	public void setBack(Node<E> back) 
	{
		this.back = back;
	}

	public E getItem() 
	{
		return item;
	}

}
