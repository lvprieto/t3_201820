package model.vo;

import java.util.Date;

/**
 * Clase que representa un viaje.
 */
public class VOTrip 
{
	/**
	 * Id del viaje.
	 */
	private int id;
	
	/**
	 * Start time del viaje.
	 */
	private Date startTime;
	
	/**
	 * End time del viaje.
	 */
	private Date endTime;
	
	
	/**
	 * Id de la bicicleta prestada.
	 */
	private int bike_id;
	
	/**
	 * Duraci�n del viaje.
	 */
	private double tripDuration;
	
	/**
	 * Id de la estaci�n de salida.
	 */
	private int fromStationId;
	
	/**
	 * Nombre de la estaci�n de salida.
	 */
	private String fromStationName;
	
	/**
	 * Id de la estaci�n de llegada.
	 */
	private int toStationId;
	
	/**
	 * Nombre de la estaci�n de llegada.
	 */
	private String toStationName;
	
	/**
	 * Tipo de usuario.
	 */
	private String usertype;
	
	/**
	 * G�nero del usuario.
	 */
	private String gender;
	
	/**
	 * A�o de nacimiento del usuario
	 */
	private String birthyear;
	
	public VOTrip( int pId ,Date pStartTime,Date pEndTime, int pBikeId , double pTripDuration , int pFromStationId , String pFromStationName , int pToStationId , String pToStationName , String pUsertype , String pGender , String pBirthyear)
	{
		id = pId;
		startTime=pStartTime;
		endTime=pEndTime;
		bike_id = pBikeId;
		tripDuration = pTripDuration;
		fromStationId = pFromStationId;
		fromStationName = pFromStationName;
		toStationId = pToStationId;
		toStationName = pToStationName;
		usertype = pUsertype;
		gender = pGender;
		birthyear = pBirthyear;
	}
	
	/**
	 * @return trip id - id of the trip.
	 */
	public int id() 
	{
		return id;
	}	
	
	/**
	 * @return start time - start time of the trip.
	 */
	public Date getStartTime() 
	{
		return startTime;
	}
	
	/**
	 * @return end time - end time of the trip.
	 */
	public Date getEndTime() 
	{
		return endTime;
	}
	
	/**
	 * @return bike id - id of the bike.
	 */
	public int getBikeId()
	{
		return bike_id;
	}
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() 
	{
		return tripDuration;
	}
	
	public int getFromStationId()
	{
		return fromStationId;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() 
	{
		return fromStationName;
	}
	
	public int getToStationId()
	{
		return toStationId;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() 
	{
		return toStationName;
	}
	
	public String getUsertype()
	{
		return usertype;
	}
	
	public String getGender()
	{
		return gender;
	}
	
	public String getBirthtear()
	{
		return birthyear;
	}
}
