package model.vo;

import java.util.Date;

/**
 * Clase que representa una estaci�n.
 * Clase copiada de la tarea 2 y modificada.
 */
public class VOStation 
{
	/**
	 * Id de la estaci�n.
	 */
	private int id;
	
	/**
	 * Nombre de la estaci�n.
	 */
	private String name;
	
	/**
	 * Ciudad de la estaci�n.
	 */
	private String city;
	
	/**
	 * Latitud de la estaci�n.
	 */
	private double latitude;
	
	/**
	 * Longitud de la estaci�n.
	 */
	private double longitude;
	
	/**
	 * Capacidad de la estaci�n.
	 */
	private int dpCapacity;
	
	/**
	 * Online date
	 */
	private Date onlineDate;
	
	/**
	 * M�todo contructor de la clase.
	 * @param pId. Id de la estaci�n. 
	 * @param pName. Nombre de la estaci�n.
	 * @param pCity. Ciudad donde est� ubicada la estaci�n.
	 * @param pLatitude. Latitud de la estaci�n.
	 * @param pLongitude. Longitud de la estaci�n.
	 * @param pDpCapacity. Capacidad de la estaci�n.
	 * @param pOnlineDate. Online date.
	 */
	public VOStation( int pId , String pName , String pCity , double pLatitude , double pLongitude , int pDpCapacity , Date pOnlineDate )
	{
		id = pId;
		name = pName;
		city = pCity;
		latitude = pLatitude;
		longitude = pLongitude;
		dpCapacity = pDpCapacity;
		onlineDate = pOnlineDate;
	}

	public int getId() 
	{
		return id;
	}

	public String getName() 
	{
		return name;
	}

	public String getCity() 
	{
		return city;
	}


	public double getLatitude() 
	{
		return latitude;
	}


	public double getLongitude() 
	{
		return longitude;
	}


	public int getDpCapacity() 
	{
		return dpCapacity;
	}
	
	public Date getOnlineDate()
	{
		return onlineDate;
	}

	public boolean equals( VOStation station )
	{
		return (id == station.id);
	}
	
}
